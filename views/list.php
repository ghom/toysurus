<?php

// PAGE DE LISTING & SORTING & FILTERING DES JOUETS

require_once '../inc/connect.php';
require_once '../functions/input.php';
require_once '../functions/list.php';
require_once '../functions/basket.php';

add_orders();

// GET DATA

$toys = [];
$current_brand = null;
$order = isset($_GET['order']) && $_GET['order'] === 'asc';

if(!$sql->connect_error){

    $query = 'SELECT * FROM `toys`';

    if(!empty($_GET) && isset($_GET['brand_id'])){
        $result = $sql->query('SELECT * FROM `brands` WHERE `id` = '.intval($_GET['brand_id']));
        if(!!$result){
            if($result->num_rows > 0){
                $current_brand = $result->fetch_object();
                $query = 'SELECT * FROM `toys` WHERE `brand_id` = '.$current_brand->id;
            }
            mysqli_free_result($result);
        }
    }

    if($order){
        $query .= ' ORDER BY `price` ASC';
    }else{
        $query .= ' ORDER BY `price` DESC';
    }

    $result = $sql->query($query);

    if(!!$result){
        if(!empty($_GET) && isset($_GET['search'])){
            $search = str_replace('+',' ',strtolower($_GET['search']));
            while($toy = $result->fetch_object()){
                if(
                    strpos( strtolower($toy->name), $search) !== FALSE ||
                    strpos( strtolower($toy->description), $search) !== FALSE
                ){
                    array_push($toys, $toy);
                }
            }
        }else{
            while($toy = $result->fetch_object()){
                array_push($toys, $toy);
            }
        }
        mysqli_free_result($result);
    }
}

// PAGES

$current_page = isset($_GET['page']) ? intval($_GET['page']) : 0;
$toy_per_page = 4;
$count_toy = count($toys) !== 0 ? count($toys) : 1;
$page_count = ceil($count_toy/$toy_per_page);
$pages = [];

for($i=0; $i<$page_count; $i++){
    array_push( $pages, array_slice($toys,$i*4,4));
}

if($current_page < 0){
    $current_page = 0;
}else if($current_page >= $page_count){
    $current_page = $page_count - 1;
}

if($page_count < 2){
    $current_page = 0;
}

$toys = $pages[$current_page];
?>

<!-- VIEW -->

<!DOCTYPE html>
<html lang="fr">
<head>
    <?php include '../inc/meta.php' ?>
    <title>
        Toys'R'Us - Liste
        <?php if(isset($current_brand)) echo ' - '.$current_brand->name; ?>
    </title>
</head>
<body>
    <div class="my-container">
        <?php include '../inc/header.php' ?>
        <main class="my-content">
            <h1 class="the-title"> Les jouets <?php if(isset($current_brand)) echo $current_brand->name; ?> </h1>
            <div class="choice">
                <form method="GET">
                    <select name="brand_id">
                        <option value="" class="grey-text">Quelle marque ?</option>
                        <?php
                            if(!$sql->connect_error){
                                $result = $sql->query('SELECT * FROM `brands`');
                                if(!!$result){
                                    while($brand = $result->fetch_object()){
                                        echo '<option value="'.$brand->id.'">'.$brand->name.'</option>';
                                    }
                                }else{
                                    echo '<option value="">⚠ Query failed</option>';
                                }
                                mysqli_free_result($result);
                            }else{
                                echo '<option value="">⚠ Connexion failed</option>';
                            }
                        ?>
                    </select>
                    <input type="submit" value="Ok">
                </form>
                <form method="GET">
                    <?php input(['order']) ?>
                    <input type="hidden" name="order" value="<?php echo $order ? 'desc' : 'asc' ?>">
                    <input type="submit" value="Tri par prix">
                    <?php if($order): ?>
                        <i class='icon ion-md-arrow-dropup'></i>
                    <?php else: ?>
                        <i class='icon ion-md-arrow-dropdown'></i>
                    <?php endif; ?>
                </form>
                <?php include '../inc/page_nav.php' ?>
            </div>
            <div class="separator"></div>
            <?php include '../inc/list.php' ?>
            <div class="choice">
                <?php include '../inc/page_nav.php' ?>
            </div>
        </main>
    </div>
</body>
</html>
<?php 

// FINISH

$sql->close();