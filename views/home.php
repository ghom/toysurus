<?php

// PAGE D'ACCUEIL - TOP 3 DES VENTES

require_once '../functions/list.php';
require_once '../inc/connect.php';

$toys = [];

if(!$sql->connect_error){

    // récupérer la somme des ventes de chaque jouet puis les trier

    $sum_result = $sql->query(
        'SELECT `toy_id`, SUM(`quantity`) AS `total` FROM `sales` GROUP BY `toy_id` ORDER BY `total` DESC LIMIT 3'
    );

    if(!!$sum_result){
        while($sum = $sum_result->fetch_object()){
            $toy_result = $sql->query("SELECT * FROM `toys` WHERE `id` = $sum->toy_id");
            if(!!$toy_result){
                array_push($toys,$toy_result->fetch_object());
                mysqli_free_result($toy_result);
            }
        }
        mysqli_free_result($sum_result);
    }

}
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <?php include '../inc/meta.php' ?>
    <title>Toys'R'Us - Home</title>
</head>
<body>
    <div class="my-container">
        <?php include '../inc/header.php' ?>
        <main class="my-content">
            <h1 class="the-title"> Top 3 des ventes </h1>
            <?php include '../inc/list.php' ?>
        </main>
    </div>
</body>
</html>
<?php $sql->close() ?>