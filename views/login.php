<?php

// PAGE DE CONNEXION ET D'INSCRIPTION

require_once '../inc/connect.php';

if($logged){
    header('Location: ./home.php');
    return;
}

// CHECK informations

$error_logs = [];


if(isset($_POST['type'])){

    if(!$sql->connect_error){
        
        
        if($_POST['type'] == 'connexion'){

            // Check email or username
            if(isset($_POST['logger'])){
                $logger = $_POST['logger'];
                $result = $sql->query("SELECT `id`, `username`, `email`, `password` FROM `users` WHERE `username` = '$logger' OR `email` = '$logger'");
                if(!!$result && $result->num_rows > 0){
                    $user = $result->fetch_object();
                    if(isset($_POST['password'])){
                        $password = hash('sha256',$_POST['password']);
                        if($password === $user->password){
                            setcookie('user', $user->password, time() + (60 * 60 * 24 * 7),'/');
                            mysqli_free_result($result);
                            header('Location: ./profil.php');
                            return;
                        }else{
                            array_push($error_logs,'Votre mot de passe est incorrect.');
                        }
                    }else{
                        array_push($error_logs,'Vous n\'avez pas utilisé le formulaire prévu à cet effet.');
                    }
                    mysqli_free_result($result);
                }else{
                    array_push($error_logs,'Votre identifiant n\'existe pas.');
                }
                
            }else{
                array_push($error_logs,'Vous n\'avez pas utilisé le formulaire prévu à cet effet.');
            }

        }else if($_POST['type'] == 'inscription'){

            if(isset($_POST['username'])){
                $username = $_POST['username'];
                if(strpos( $username, " ") === FALSE){
                    $result = $sql->query("SELECT `id` AS `count` FROM `users` WHERE `username` = '$username'");
                    if(!!$result && $result->num_rows === 0){
                        mysqli_free_result($result);
                        if(!empty($_POST['firstname'])){
                            $firstname = $_POST['firstname'];
                            if(!empty($_POST['lastname'])){
                                $lastname = $_POST['lastname'];
                                if(isset($_POST['email'])){
                                    $email = $_POST['email'];
                                    if(preg_match('/^[^\s]+@[^\s]+\.[^\s]+$/i',$email)){
                                        $result = $sql->query("SELECT `id` AS `count` FROM `users` WHERE `email` = '$email'");
                                        if(!!$result && $result->num_rows === 0){
                                            mysqli_free_result($result);
                                            if(!empty($_POST['password'])){
                                                $password = $_POST['password'];
                                                if(strlen($password) > 7){
                                                    if(!preg_match('/\s/i',$password)){
                                                        if(!empty($_POST['confirmation'])){
                                                            $confirm = $_POST['confirmation'];
                                                            if(hash('sha256',$password) === hash('sha256',$confirm)){

                                                                // INJECT NEW USER

                                                                $hash = hash('sha256',$password);

                                                                $success = $sql->query("INSERT INTO `users` (
                                                                    `username`, 
                                                                    `password`, 
                                                                    `email`, 
                                                                    `firstname`, 
                                                                    `lastname`
                                                                ) VALUE (
                                                                    '$username', 
                                                                    '$hash',
                                                                    '$email',
                                                                    '$firstname',
                                                                    '$lastname'
                                                                )");

                                                                if($success){

                                                                    $result = $sql->query("SELECT `password` FROM `users` WHERE `username` = '$username'");

                                                                    if(!!$result && $result->num_rows > 0){
                                                                        $user = $result->fetch_object();
                                                                        mysqli_free_result($result);
                                                                        setcookie('user', $user->password, time() + (60 * 60 * 24 * 7),'/');
                                                                        header('Location: ./profil.php');
                                                                        return;
                                                                    }else{
                                                                        array_push($error_logs,'Nous n\'avons pas réussi a vous connecter...');
                                                                    }
                                                                }else{
                                                                    array_push($error_logs,'Nous n\'avons pas réussi a vous inscrire...');
                                                                }
                                                            }else{
                                                                array_push($error_logs,'La confirmation du mot de passe à échouée.');
                                                            }
                                                        }else{
                                                            array_push($error_logs,'Vous devez confirmer votre mot de passe.');
                                                        }
                                                    }else{
                                                        array_push($error_logs,'Votre mot de passe ne doit pas contenir d\'espace.');
                                                    }
                                                }else{
                                                    array_push($error_logs,'Votre mot de passe doit faire au moins 8 charactères.');
                                                }
                                            }else{
                                                array_push($error_logs,'Vous devez entrer un mot de passe.');
                                            }
                                        }else{
                                            array_push($error_logs,'Votre email est déjà utilisé.');
                                        }
                                    }else{
                                        array_push($error_logs,'Votre email est incorrect.');
                                    }
                                }else{
                                    array_push($error_logs,'Vous devez renseigner votre adresse email.');
                                }
                            }else{
                                array_push($error_logs,'Vous devez renseigner votre nom de famille.');
                            }
                        }else{
                            array_push($error_logs,'Vous devez renseigner votre prénom.');
                        }
                    }else{
                        array_push($error_logs,'Votre pseudo est déjà utilisé.');
                    }
                }else{
                    array_push($error_logs,'Votre pseudo ne doit pas comporter d\'espace.');
                }
            }
        }
    }
}

?>

<!-- VIEW -->

<!DOCTYPE html>
<html lang="fr">
<head>
    <?php include '../inc/meta.php' ?>
    <title>
        Toys'R'Us - Connexion
    </title>
</head>
<body>
    <?php // var_dump($_POST) ?>
    <div class="my-container">
        <?php include '../inc/header.php' ?>
        <main class="my-content">

            <!-- ERRORS -->

            <?php foreach($error_logs as $error_log): ?>
                <div class="error">
                    ⚠ <?php echo $error_log ?>
                </div>
            <?php endforeach; ?>

            <!-- CONNECTION -->

            <h2 class="the-title">Connexion</h2>

            <form method="post" class="form shadow">
                <input type="text" name="logger" placeholder="Email ou pseudo">
                <input type="password" name="password" placeholder="Mot de passe">
                <input type="hidden" name="type" value="connexion">
                <input type="submit" value="Se connecter">
            </form>

            <!-- INSCRIPTION -->

            <h2 class="the-title">Inscription</h2>

            <form method="post" class="form shadow">
                <input type="text" name="username" placeholder="Pseudo">
                <input type="text" name="firstname" placeholder="Prénom">
                <input type="text" name="lastname" placeholder="Nom">
                <input type="text" name="email" placeholder="Email">
                <input type="password" name="password" placeholder="Mot de passe">
                <input type="password" name="confirmation" placeholder="Confirmation">
                <input type="hidden" name="type" value="inscription">
                <input type="submit" value="S'inscrire">
            </form>

        </main>
    </div>
</body>
</html>