<?php

// PAGE DE PROFIL

require_once '../inc/connect.php';
require_once '../functions/basket.php';

if(!$logged){
    header('Location: ./login.php');
    return;
}

$user = false;
$password = $_COOKIE['user'];

if(!$sql->connect_error){
    $result = $sql->query("SELECT * FROM `users` WHERE `password` = '$password'");
    if(!!$result && $result->num_rows > 0){
        $user = $result->fetch_object();
        mysqli_free_result($result);
    }
}

if(!$user){
    header('Location: ./login.php');
    return;
}

if(isset($_GET['logout'])){
    session_destroy();
    setcookie('user', null, -1, '/');
    unset($_COOKIE['user']);
    unset($_SESSION);
    header('Location: ./login.php');
    return;
}

$basket = get_basket($sql);
$basket_price = 0;

foreach($basket as $order){
    $basket_price += floatval($order['toy']->price) * $order['quantity'];
}

?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <?php include '../inc/meta.php' ?>
    <title>
        Toys'R'Us - Profil de
        <?php echo $user->username ?>
    </title>
</head>
<body>
    <div class="my-container">
        <?php include '../inc/header.php' ?>
        <main class="my-content">
            <h1 class="the-title">
                Profil de <?php echo $user->username ?>
            </h1>
            <div class="bicol">
                <div class="col-left">
                    <div class="cadre shadow">
                        <img class="avatar" src="../images/user.jpg" alt="logo toysrus giraffe">
                    </div>
                </div>
                <div class="col-right">
                    <div>
                        <div class="separator"></div>
                        <h2> Informations personnelles : </h2>
                        <ul class="informations">
                            <li><?php echo $user->firstname.' '.$user->lastname ?></li>
                            <li><?php echo $user->email ?></li>
                        </ul>
                    </div>
                    <div class="panier">
                        <div class="separator"></div>
                        <h2> Votre panier : </h2>
                        <p>
                            <?php if(!empty($basket)): ?>
                                <table class="shadow">
                                    <thead>
                                        <tr>
                                            <th> Quantité </th>
                                            <th> Nom </th>
                                            <th> Magasin </th>
                                            <th> Prix </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($basket as $order): ?>
                                            <tr>
                                                <td class="border-dotted"><?php echo $order['quantity'] ?><span class="bold"> x </span></td>
                                                <td class="border-dotted"><?php echo $order['toy']->name ?></td>
                                                <td class="border-dotted"><?php echo $order['store'] ? $order['store']->name : '❌' ?></td>
                                                <td class="border-dotted"><span class="bold"><?php echo str_replace('.',',',floatval($order['toy']->price)*$order['quantity']) ?> € </span></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="2"></td>
                                            <td colspan="2"><span class="bold"> Total : <?php echo str_replace('.',',',strval($basket_price)) ?> € </span></td>
                                        </tr>
                                    </tfoot>
                                </table>
                            <?php else: ?>
                                Vous n'avez pas encore ajouté de jouet, <a href="./list.php" class="blue-text"> en choisir un maintenant ? </a>
                            <?php endif; ?>
                        </p>
                    </div>
                    <div class="separator"></div>
                    <form method="GET" class="big-button">
                        <input type="hidden" name="logout" value="1">
                        <input type="submit" value="Se déconnecter">
                    </form>
                </div>
            </div>
        </main>
    </div>
</body>
</html>