<?php

// PAGE DE DETAIL D'UN JOUET

require_once '../inc/connect.php';
include_once '../functions/input.php';
include_once '../functions/basket.php';

add_orders();

$toy = false;
$brand_name = false;
$store_id = false;
$stock = false;
$stores = [];

if(!$sql->connect_error){

    $result = false;

    if(!empty($_GET)){
        if(isset($_GET['toy_id'])){
            $result = $sql->query('SELECT * FROM `toys` WHERE `id` = '.intval($_GET['toy_id']));
            if(!!$result && $result->num_rows > 0){
                $toy = $result->fetch_object();
            }
        }
        $query = null;
        if( isset($_GET['store_id']) &&isset($_GET['toy_id']) ){
            $query = 'SELECT `quantity` AS `total` FROM `stock` WHERE `toy_id` = '.$toy->id.' AND `store_id` = '.intval($_GET['store_id']);
        }else{
            $query = 'SELECT SUM(`quantity`) AS `total` FROM `stock` WHERE `toy_id` = '.$toy->id;
        }
        $result = $sql->query($query);
        if(!!$result && $result->num_rows > 0){
            $store_id = isset($_GET['store_id']) ? $_GET['store_id'] : false;
            $stock = $result->fetch_object()->total;
        }
    }

    $result = $sql->query('SELECT `name`,`id` FROM `stores`');
    if(!!$result){
        while($store = $result->fetch_object()){
            array_push($stores,$store);
        }
    }

    if(!!$toy){
        $result = $sql->query("SELECT `name`,`id` FROM `brands` WHERE `id` = $toy->brand_id");
        if(!!$result){
            $brand = $result->fetch_object();
            if(!!$brand){
                $brand_name = $brand->name;
            }
        }
    }

    if(!!$result){
        mysqli_free_result($result);
    }
}

?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <?php include '../inc/meta.php' ?>
    <title>
        Toys'R'Us - Détails
        <?php if(!!$toy) echo ' - '.$toy->name; ?>
    </title>
</head>
<body>
    <?php //var_dump($_SERVER); ?>
    <div class="my-container">
        <?php include '../inc/header.php' ?>
        <main class="my-content">
            <?php if(!!$toy): ?>
                <h1 class="name"><?php echo $toy->name ?></h1>
                <div class="bicol">
                    <div class="col-left">
                        <div class="cadre shadow cadre-anim">
                            <img class="produit" src="../images/<?php echo $toy->image ?>" alt="">
                        </div>
                        <h2 class="price">
                            <?php echo $toy->price ?> €
                        </h2>
                        <form method="GET">
                            <select class="store" name="store_id">
                                <option value="" class="grey-text">Quel magasin ?</option>
                                <?php foreach($stores as $store): ?>
                                    <option value="<?php echo $store->id ?>" <?php echo $store_id === $store->id ? 'selected="selected"' : '' ?>><?php echo $store->name ?></option>
                                <?php endforeach; ?>
                            </select>
                            <input type="hidden" name="toy_id" value="<?php echo $toy->id ?>">
                            <input type="submit" value="Ok">
                        </form>
                        <div class="stock bold">
                            <span class="blue-text">Stock:</span> <?php echo $stock !== false ? $stock : 'inconnu' ?>
                        </div>
                        <?php
                            if(!!$store_id){
                                basket_button($toy->id, $store_id,'Ajouter au panier');
                            }
                        ?>
                    </div>
                    <div class="col-right">
                        <div class="marque bold">
                            <span class="blue-text">Marque:</span> <?php echo $brand_name !== false ? $brand_name : 'inconnue' ?>
                        </div>
                        <div class="description">
                            <?php echo $toy->description ?>
                        </div>
                    </div>
                </div>
            <?php else: ?>
                <h1>Le jouet demandé n'existe pas dans nos magasins :(</h1>
            <?php endif ?>
        </main>
    </div>
</body>
</html>
<?php $sql->close();