<?php 

// AJOUTE LES JOUETS EN GET AU PANIER

function add_orders() : void
{
    if(isset($_GET['add_toy_id']) && isset($_GET['add_store_id'])){
        $toy_id = $_GET['add_toy_id'];
        $store_id = $_GET['add_store_id'];
        unset(
            $_GET['add_toy_id'],
            $_GET['add_store_id']
        );
        if(!isset($_SESSION['basket'])){
            $_SESSION['basket'] = [];
        }
        foreach($_SESSION['basket'] as &$order){
            if(
                $order['toy_id'] == $toy_id &&
                $order['store_id'] == $store_id
            ){
                $order['quantity'] ++;
                return;
            }
        }
        array_push($_SESSION['basket'],[
            'toy_id' => $toy_id,
            'store_id' => $store_id,
            'quantity' => 1
        ]);
    }
}

// RETURN UN TABLEAU EXPLOITABLE QUI CONTIENT LE PANIER

function get_basket($sql) : array 
{

    $basket = [];

    if(!$sql->connect_error && isset($_SESSION['basket'])){
        foreach($_SESSION['basket'] as $order){
            $new_order = [];
            $toy_result = $sql->query(
                'SELECT * FROM `toys` WHERE `id` = '.$order['toy_id']
            );
            if(!!$toy_result && $toy_result->num_rows > 0){
                $new_order['toy'] = $toy_result->fetch_object();
                mysqli_free_result($toy_result);
            }
            if($order['store_id'] > 0){
                $store_result = $sql->query(
                    'SELECT * FROM `stores` WHERE `id` = '.$order['store_id']
                );
                if(!!$store_result && $store_result->num_rows > 0){
                    $new_order['store'] = $store_result->fetch_object();
                    mysqli_free_result($store_result);
                }
            }else{
                $new_order['store'] = false;
            }
            $new_order['quantity'] = $order['quantity'];
            array_push($basket,$new_order);
        }
    }

    return $basket;
}