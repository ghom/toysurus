<?php

// CONVERTI LES "GET" EXISTANTS EN INPUT:HIDDEN
// EXCLU UN "GET"

function input(array $excludes):void
{
    foreach($_GET as $name => $value){
        if(!in_array($name,$excludes)){
            echo '<input type="hidden" name="'.$name.'" value="'.$value.'">';
        }
    }
}

function basket_button( $toy_id, $store_id=0, $message=""){
    global $logged;
    $added = false;
    if(strlen($message) > 0){
        $message = ' '.$message;
    }
    if(isset($_SESSION['basket'])){
        foreach($_SESSION['basket'] as $order){
            if(
                $order['toy_id'] == $toy_id &&
                $order['store_id'] == $store_id
            ){
                $added = $order;
                break;
            }
        }
    }
    if($logged){
        echo '<div class="basket-button">';
            echo '<form method="get" action="#toy'.$toy_id.'">';
                input(['add_toy_id','add_store_id']);
                echo '<input type="hidden" name="add_toy_id" value="'.$toy_id.'">';
                echo '<input type="hidden" name="add_store_id" value="'.$store_id.'">';
                echo '<input type="submit" value="'.$message.(!!$added ? '➕ ('.$added['quantity'].')' : '🛒').'">';
            echo '</form>';
        echo '</div>';
    }
}