
<?php

// AFFICHE UNE CARTE DE JOUET

include_once '../functions/input.php';

function add_card($toy):void
{
    echo '<a class="shadow" id="toy'.$toy->id.'" href="../views/detail.php?toy_id='.$toy->id.'">';
        echo '<div class="my-card">';
            echo '<div class="my-card-cadre">';
                echo '<img class="my-card-image" src="../images/'.$toy->image.'">';
            echo '</div>';
            echo '<div class="my-card-name">';
                echo $toy->name;
            echo '</div>';
            echo '<div class="my-card-price">';
                echo str_replace('.',',',strval($toy->price)).' €';
                basket_button($toy->id);
            echo '</div>';
        echo '</div>';
    echo '</a>';
}