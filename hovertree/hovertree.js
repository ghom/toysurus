
/*

    Framework JS/CSS pour mon menu déroulant 😇
                  _______           _______  _______   _________ _______  _______  _______ 
        |\     /|(  ___  )|\     /|(  ____ \(  ____ )  \__   __/(  ____ )(  ____ \(  ____ \
        | )   ( || (   ) || )   ( || (    \/| (    )|     ) (   | (    )|| (    \/| (    \/
        | (___) || |   | || |   | || (__    | (____)|     | |   | (____)|| (__    | (__    
        |  ___  || |   | |( (   ) )|  __)   |     __)     | |   |     __)|  __)   |  __)   
        | (   ) || |   | | \ \_/ / | (      | (\ (        | |   | (\ (   | (      | (      
        | )   ( || (___) |  \   /  | (____/\| ) \ \__     | |   | ) \ \__| (____/\| (____/\
        |/     \|(_______)   \_/   (_______/|/   \__/     )_(   |/   \__/(_______/(_______/

                ht              // hovered drop down menu
                ht-up           // hovered drop up menu
                ht-right        // hovered drop down menu to right
                ht-left         // hovered drop down menu to left

                ht-click        // clickable drop down menu
                ht-click-up     // clickable drop up menu
                ht-click-right  // clickable drop down menu to right
                ht-click-left   // clickable drop down menu to left

                ht-items        // item container for hide or show items
                ht-item         // menu item
                ht-close        // close click-menu

            # Add -num after class name for identify sub-menus. (from 1 to 4)
            # Give the same identifiers to his container and his items.
            # example : ht-right-2

*/

if(typeof jQuery === 'undefined'){
    throw Error('You must to include version 3.4.1 of jQuery : https://code.jquery.com/jquery-3.4.1.min.js')
}

$(() => {
    for(let i=0; i<5; i++){
        const ht_hover = `
            .ht${i?'-'+i:''},
            .ht-down${i?'-'+i:''}, 
            .ht-up${i?'-'+i:''},
            .ht-left${i?'-'+i:''},
            .ht-right${i?'-'+i:''}
        `
        const ht_click = `
            .ht-click${i?'-'+i:''},
            .ht-click-down${i?'-'+i:''}, 
            .ht-click-up${i?'-'+i:''},
            .ht-click-left${i?'-'+i:''},
            .ht-click-right${i?'-'+i:''}
        `
        const ht_items = `.ht-items${i?'-'+i:''}`
        const ht_close = `.ht-close${i?'-'+i:''}`

        $(ht_hover).hover(
            event => $(event.target).find(ht_items).show(),
            event => $(event.target).find(ht_items).hide()
        )
        $(ht_click).click(
            event => {
                if($(event.target).toggleClass('ht-open').hasClass('ht-open')){
                    $(event.target).find(ht_items).show()
                }else{
                    $(event.target).find(ht_items).hide()
                }
            }
        )
        // $(ht_close).click(
        //     event => {
        //         if(!$(event.target).is(ht_click)){
        //             $(ht_click).find(ht_items).hide()
        //         }
        //     }
        // )
    }
})

