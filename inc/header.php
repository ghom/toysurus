
<!-- AFFICHE LE HEADER -->

<header>
    <div class="logo">
        <a href="../views/home.php"><img src="../images/logo.png" alt="Logo de la boutique"></a>
    </div>
    <nav class="shadow">
        <ul>
            <a href="../views/list.php">
                <li class="menu">
                    Tous les jouets
                </li>
            </a>
            <li class="menu ht">
                Par marque <i class='icon ion-md-arrow-dropdown'></i>
                <div class="ht-items">
                    <?php
                        if(!$sql->connect_error){
                            $result = $sql->query('SELECT `id`, `name` FROM `brands`');
                            if(!!$result){
                                while($brand = $result->fetch_object()){
                                    $result_count = $sql->query('SELECT COUNT(`id`) as `count` FROM `toys` WHERE `brand_id` = '.$brand->id);
                                    if(!!$result_count){
                                        $count = $result_count->fetch_object()->count;
                                        echo '<a href="../views/list.php?brand_id='.$brand->id.'"><div class="ht-item">'.$brand->name.' ('.$count.')</div></a>';
                                        mysqli_free_result($result_count);
                                    }
                                }
                                mysqli_free_result($result);
                            }else{
                                echo '<div class="ht-item">⚠ Query failed</div>';
                            }
                        }else{
                            echo '<div class="ht-item">⚠ Connexion failed</div>';
                        }
                    ?>
                </div> 
            </li>
        </ul>
        <form class="search" action="../views/list.php" method="GET">
            <input type="text" name="search" placeholder="Chercher un jouet">
            <input type="submit" value="Go !">
        </form>
        <div class="log-card shadow">
            <?php if($logged): ?>
            <a href="../views/profil.php"> 👥 Mon profil </a>
            <?php else: ?>
            <a href="../views/login.php"> 👥 Connexion </a>
            <?php endif; ?>
        </div>
    </nav>
</header>

<?php // if(isset($_COOKIE['user'])) var_dump($_COOKIE['user']) ?>