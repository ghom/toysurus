
<!-- AJOUTE UN NAVIGATEUR DE PAGINATION -->

<div class="pages shadow">
    <form method="get">
        <?php input(['page']) ?>
        <input type="hidden" name="page" value="0">
        <input type="submit" value="<<">
    </form>
    <form method="get">
        <?php input(['page']) ?>
        <input type="hidden" name="page" value="<?php echo $current_page - 1 ?>">
        <input type="submit" value="<">
    </form>
    <span class="pages-text">Page <?php echo $current_page + 1 ?> sur <?php echo $page_count ?></span>
    <form method="get">
        <?php input(['page']) ?>
        <input type="hidden" name="page" value="<?php echo $current_page + 1 ?>">
        <input type="submit" value=">">
    </form>
    <form method="get">
        <?php input(['page']) ?>
        <input type="hidden" name="page" value="<?php echo $page_count - 1 ?>">
        <input type="submit" value=">>">
    </form>
</div>